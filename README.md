<div align="center">  

  [![website](https://gitlab.com/eggerd/eggerd/-/raw/main/assets/btn-website.jpg)](https://dustin-eckhardt.de) &nbsp; 
  [![mail](https://gitlab.com/eggerd/eggerd/-/raw/main/assets/btn-mail.jpg)](mailto:dustin-eckhardt@online.de) &nbsp;
  [![github](https://gitlab.com/eggerd/eggerd/-/raw/main/assets/btn-github.jpg)](https://github.com/Eggerd) &nbsp;
  [![twitter](https://gitlab.com/eggerd/eggerd/-/raw/main/assets/btn-twitter.jpg)](https://twitter.com/Eggerd) &nbsp;
  [![linkedin](https://gitlab.com/eggerd/eggerd/-/raw/main/assets/btn-linkedin.jpg)](https://www.linkedin.com/in/dustin-eckhardt/)

  <br>

  <a><img src="https://gitlab.com/eggerd/eggerd/-/raw/main/assets/ico-merge.png" width="24px"></a>
  **Fix some private contributions being hidden on the contribution calendar**
  <small>  
    [GitLab !74826](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/74826) · 
    [v14.6](https://about.gitlab.com/releases/2021/12/22/gitlab-14-6-released) · 
    Technically my first & kinda proud of this one <sup>(◠‿◠)</sup>
  </small>

</div>
